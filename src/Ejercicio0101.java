/*
 * * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
// static int m = 5; // variable miembro
class Ejercicio0201 {

  static int m = 5; // variable miembro

  public static void main(String args[]) {
    int l;  // variable local
    l = m + 2;
    System.out.println("Suma: " + l);
  }
}
