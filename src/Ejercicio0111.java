/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-oct-2013
 */
class Ejercicio0111 {

  public static void main(String[] args) {
    char car;
    boolean consonante;
    double a;
    for (int i = 0; i < 100; i++) {
      consonante = true;
      /* Math.random() devuelve valor double
       * aleatorio entre 0.0 y 1.1
       */
      a = Math.random() * 26 + 'A';
      car = (char) (a);
      switch (car) {
        case 'A':
          consonante = false;
          break;
        case 'E':
          consonante = false;
          break;
        case 'I':
          consonante = false;
          break;
        case 'O':
          consonante = false;
          break;
        case 'U':
          consonante = false;
          break;
      }
      System.out.print("Generado " + car + " -> ");
      if (consonante) {
        System.out.println("consonante");
      } else {
        System.out.println("vocal");
      }
    }
  }
}
/* EJECUCION:
Generado N -> consonante
Generado I -> vocal
Generado J -> consonante
Generado M -> consonante
Generado B -> consonante
Generado N -> consonante
Generado L -> consonante
Generado T -> consonante
Generado Y -> consonante
Generado L -> consonante
Generado B -> consonante
Generado V -> consonante
Generado C -> consonante
Generado K -> consonante
Generado O -> vocal
Generado V -> consonante
*/
