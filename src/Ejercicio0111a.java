/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-oct-2013
 */
class Ejercicio0111a {

  public static char letra() {
    return (char) (Math.random() * 26 + 'a');
  }

  public static void main(String[] args) {

    boolean consonante;

    //dara true=consonante y false=vocal
    consonante = (letra() != 'a') & (letra() != 'e') & (letra() != 'i')
            & (letra() != 'o') & (letra() != 'u');

    if (consonante == true) {
      System.out.print("La letra " + letra() + " es una consonante.\n");
    } else {
      System.out.print("La letra " + letra() + " es una vocal.\n");
    }

  }
}
/* EJ:
La letra n es una consonante.
*/
